export const BASE_URL = 'http://localhost:5555';
export const coursesPerPage = 9;
export const STUDENT_ROLE = 'Student';
export const TEACHER_ROLE = 'Teacher';
