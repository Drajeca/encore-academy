import decode from "jwt-decode";

export const getUserFromToken = (token) => {
  try {
    const user = decode(token);
    return user;
  } catch {
    localStorage.removeItem("token");
  }

  return null;
};
