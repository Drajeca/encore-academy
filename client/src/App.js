
import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';
import About from './components/About/About';
import AllCourses from './components/Courses/AllCourses/AllCourses';
import CreateCourse from './components/Courses/CreateCourse/CreateCourse';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import Register from './components/Register/Register';
import AllStudents from './components/Users/AllStudents/AllStudents';
import EditUser from './components/Users/EditUser/EditUser';
import AuthContext from './providers/AuthContext';
import AllSections from './components/Sections/AllSections/AllSections';
import CreateSection from './components/Sections/CreateSection/CreateSection';
import EditSection from './components/Sections/EditSection/EditSection';
import SectionDetails from './components/Sections/SectionDetails/SectionDetails';
import { getUserFromToken } from './utils/token';
import EditCourse from './components/Courses/EditCourse/EditCourse';
import TeacherEnroll from './components/Courses/TeacherEnroll/TeacherEnroll';
import GuardedRoute from './providers/GuardedRoute';
import AllTeachers from './components/Users/AllTeachers/AllTeachers';
import { TEACHER_ROLE } from './common/constants';
import SingleUserDetails from './components/Users/SingleStudentDetails/SingleUserDetails';
import NotFound from './components/NotFound/NotFound';

const App = () => {
  const [user, setUser] = useState(getUserFromToken(localStorage.getItem('token')));

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ user, setUser }}>
        <Header />
        <Switch>
          <Redirect exact path="/" to="/home" /> 
          <Route exact path="/home" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/courses" component={AllCourses} />
          <GuardedRoute exact path="/courses/create" component={CreateCourse} auth={user?.role === TEACHER_ROLE} />
          <Route exact path='/register' component={Register} />
          <GuardedRoute exact path='/users' component={AllStudents} auth={!!user} />
          <GuardedRoute exact path='/users/teachers' component={AllTeachers} auth={!!user} />
          <GuardedRoute exact path='/users/:id' component={SingleUserDetails} auth={!!user} />
          <GuardedRoute exact path='/users/:id/edit' component={EditUser} auth={!!user} />
          <GuardedRoute exact path="/courses/:courseId/sections" component={AllSections} auth={!!user} />
          <GuardedRoute exact path="/courses/:courseId/sections/new" component={CreateSection} auth={user?.role === TEACHER_ROLE} />
          <GuardedRoute exact path="/courses/:courseId/sections/:sectionId/edit" component={EditSection} auth={user?.role === TEACHER_ROLE} />
          <GuardedRoute exact path="/courses/:courseId/sections/:sectionId" component={SectionDetails} auth={!!user} />
          <GuardedRoute exact path="/courses/:courseId/edit" component={EditCourse} auth={user?.role === TEACHER_ROLE} />
          <GuardedRoute exact path="/courses/:courseId/enroll" component={TeacherEnroll} auth={user?.role === TEACHER_ROLE} />
          <Route path="*" component={NotFound} />
        </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
