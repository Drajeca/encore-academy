import { BASE_URL } from "../common/constants";

export const getAllStudents = () => 
   fetch(`${BASE_URL}/users`, {
      method: 'GET',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
   }).then(res => res.json());

export const getAllTeachers = () => 
   fetch(`${BASE_URL}/users/teachers`, {
      method: 'GET',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
   }).then(res => res.json());


export const getUserById = (id) => 
   fetch(`${BASE_URL}/users/${id}`, {
      method: 'GET',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
   }).then(res => res.json());


export const createUser = (email, firstName, lastName, password) => 
     fetch(`${BASE_URL}/users`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email,
            firstName,
            lastName,
            password,
          })
        })
        .then(res => res.json());


export const updateUser = (id, firstName, lastName, password) => 
   fetch(`${BASE_URL}/users/${id}`, {
      method: 'PUT',
      headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({firstName, lastName, password})
   })
   .then(res => res.json());

export const deleteUser = (id) => 
   fetch(`${BASE_URL}/users/${id}`, {
      method: 'DELETE',
      headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
  })
  .then(res => res.json());

  export const selfEnroll = (courseId) =>
  fetch(`${BASE_URL}/courses/${courseId}/enroll`, {
     method: 'POST',
     headers: {
     'Content-Type': 'application/json',
     'Authorization': `Bearer ${localStorage.getItem('token')}`
     },
  })
  .then(res => res.json());

  export const selfUnenroll = (courseId) =>
  fetch(`${BASE_URL}/courses/${courseId}/unenroll`, {
     method: 'DELETE',
     headers: {
     'Content-Type': 'application/json',
     'Authorization': `Bearer ${localStorage.getItem('token')}`
     },
  })
  .then(res => res.json());
    

  export const teacherEnroll = (courseId, data) =>
  fetch(`${BASE_URL}/courses/${courseId}/enroll/teacher`, {
     method: 'POST',
     headers: {
     'Content-Type': 'application/json',
     'Authorization': `Bearer ${localStorage.getItem('token')}`
     },
     body: JSON.stringify(data)
  })
  .then(res => res.json());

  
  export const teacherUnenroll = (courseId, data) =>
  fetch(`${BASE_URL}/courses/${courseId}/unenroll/teacher`, {
     method: 'DELETE',
     headers: {
     'Content-Type': 'application/json',
     'Authorization': `Bearer ${localStorage.getItem('token')}`
     },
     body: JSON.stringify(data)
  })
  .then(res => res.json());
