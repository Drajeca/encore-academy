import { BASE_URL } from "../common/constants";

export const getAllCourseSections = (courseId) => fetch(`${BASE_URL}/courses/${courseId}/sections`)
.then((r) => r.json());

export const createNewSection = (sectionData, courseId) =>
  fetch(`${BASE_URL}/courses/${courseId}/sections`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify(sectionData),
  }).then((r) => r.json());


  export const deleteSection = (courseId, sectionId) =>
    fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, {
        method: "DELETE",
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "Application/json",
        },
  }).then((r) => r.json());

  export const getSection = (courseId, sectionId) =>
    fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`)
    .then(r => r.json());


    export const updateSectionEntry = (courseId, sectionId, sectionData) =>
    fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "Application/json",
      },
      body: JSON.stringify(sectionData),
    }).then((r) => r.json());
   
    export const updateOrder = ({ order1, order2 }, courseId) => 
    fetch(`${BASE_URL}/courses/${courseId}/sections/order`, {
       method: 'POST',
       headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,  
       'Content-Type': 'application/json',
       },
       body: JSON.stringify({
           order1,
           order2,
         })
       })
       .then(res => res.json());
