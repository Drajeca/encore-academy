import { BASE_URL } from "../common/constants";

export const getAllCourses = () => fetch(`${BASE_URL}/courses`).then((r) => r.json());

export const getCourseById = (id) =>
     fetch(`${BASE_URL}/courses/${id}`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
     })
     .then(res => res.json());

export const createCourse = (title, description) =>
     fetch(`${BASE_URL}/courses`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(title, description)
     })
     .then(res => res.json());

export const updateCourse = (id, data) => 
     fetch(`${BASE_URL}/courses/${id}`, {
         method: 'PUT',
         headers: {
         'Content-Type': 'application/json',
         'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
         body: JSON.stringify(data)
      })
      .then(res => res.json());

     
export const deleteCourse = (id) =>
      fetch(`${BASE_URL}/courses/${id}`, {
         method: 'DELETE',
         headers: {
         'Content-Type': 'application/json',
         'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      })
      .then(res => res.json());


     export const getEnrolledStudents = (courseId) =>
     fetch(`${BASE_URL}/courses/${courseId}/enroll`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
     })
     .then(res => res.json());
   
    export const getAllUnenrolled = (courseId) =>
    fetch(`${BASE_URL}/courses/${courseId}/unenrolled`, {
       method: 'GET',
       headers: {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${localStorage.getItem('token')}`
       },
    })
    .then(res => res.json());


export const getAllVisibleStudentCourses = () =>
fetch(`${BASE_URL}/courses/visible/students`, {
   method: 'GET',
       headers: {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${localStorage.getItem('token')}`
       },
   })
   .then(res => res.json());

