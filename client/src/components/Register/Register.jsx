import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { createUser } from '../../requests/users';
import registerValidations from '../../validators/register-validations';
import Background from '../../wallpapers/register-wallpaper2.jpg'
import './Register.css'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';

export const Register = () => {
    const [error, setError] = useState('');
    const history = useHistory();
    
    const [isFormValid, setIsFormValid] = useState(false);
    const [credentials, setCredentials] = useState({
         email: {
            value: '',
            type: 'text',
            placeholder: 'Email',
            valid: true,
            touched: false,
            fieldReq: "Must be a valid email account",
         },
         firstName: {
            value: '',
            type: 'text',
            placeholder: 'First name',
            valid: true,
            touched: false,
            fieldReq: "Between 3 and 20 symbols, including letters and digits",
         },
         lastName: {
            value: '',
            type: 'text',
            placeholder: 'Last name',
            valid: true,
            touched: false,
            fieldReq: "Between 3 and 20 symbols, including letters and digits",
         },
         password: {
            value: '',
            type: 'password',
            placeholder: 'Password',
            valid: true,
            touched: false,
            fieldReq: "Between 4 and 10 symbols, must include both letters and digits",
         },
    }); 

    const handleInputChange = (e) => {
        const {name, value} = e.target;

        const updatedInput = credentials[name];
        updatedInput.value = value;
        updatedInput.valid = registerValidations[name](value);
        updatedInput.touched = true;

        setCredentials({...credentials, [name]: updatedInput});

        const formValid = Object.values(credentials).every((element) => element.valid && element.touched);
        
        setIsFormValid(formValid);

    };
    const useStyles = makeStyles(() =>
      createStyles({
        root: {
          "& .MuiTextField-root": {
            width: "50ch",
            marginBottom: "1ch",
          },
        },
      })
    );

    const classes = useStyles();

    const inputFields = Object.entries(credentials).map(([key, element]) => {
      return (
        // <Form.Group className='mb-3' controlId={`ControlInput-${key}`} key={key}>
        //   <Form.Control
        //     style={element.valid ? 
        //       { border: 'solid rgb(15, 120, 75) 1px',
        //         borderRadius: '5px',
        //         boxShadow: '2px 2px 2px rgb(23, 88, 32)',
        //         marginBottom: '10px',
        //       } 
        //       : 
        //       { border: "1.5px solid red",
        //         borderRadius: '5px',
        //         boxShadow: '2px 2px 2px rgb(23, 88, 32)',
        //         marginBottom: '10px', }}
        //     name={key}
        //     type={element.type}
        //     placeholder={element.placeholder}
        //     value={element.value}
        //     onChange={handleInputChange}
        //   />
        //   <div style={{ color: "gray" }}>{element.fieldReq}</div>
        // </Form.Group>
        <div className={classes.root} noValidate autoComplete="off" key={key}>
          <div>
            <TextField 
              id={key}
              label={element.placeholder} 
              value={element.value}
              helperText={element.fieldReq}
              type={element.type}
              onChange={handleInputChange}
              name={key}
              error={!element.valid}
            />
          </div>
        </div>
      )
    });

    const cancel = () => history.push('/home')
  
    const triggerRegister = (e) => {
      e.preventDefault();

      if (!isFormValid) {
        console.log("Form is not valid!");
        return;
      } 

      createUser(
        credentials.email.value, 
        credentials.firstName.value, 
        credentials.lastName.value, 
        credentials.password.value)
        .then((data) => {
          if (data.message) {
            return alert(data.message);
          }
          
        })
        .catch((error) => setError(error.message))
        .then(history.push('/home'))
  
        if (error) {
          return (
            <h4>
              <i>An error has occurred: </i>
              {error}
            </h4>
          );
        }
    };
  
    return (
      <div className="container-register">
        <div
          style={{
            backgroundImage: `url(${Background})`,
            position: "absolute",
            width: "100%",
            height: "100vh",
            backgroundPosition: "center",
            backgroundSize: "cover",
          }}
        >
          <div className="register-form">
            <Form onSubmit={triggerRegister}>

              <h3 className="register-text">Register</h3>
              {inputFields}
              <br />
              <div className="register-btns-group">
                <Button
                  type="submit"
                  variant="success"
                  id="confirm-register"
                  disabled={!isFormValid}
                >
                  Register
                </Button>
                <Button
                  variant="danger"
                  id="cancel-register"
                  onClick={cancel}
                >
                  Cancel
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    );
  };
  
  export default Register;
