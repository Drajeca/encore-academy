import React from 'react';
import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { getAllUnenrolled, getCourseById, getEnrolledStudents } from '../../../requests/courses';
import { teacherEnroll, teacherUnenroll } from '../../../requests/users';
import './TeacherEnroll.css'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { AiOutlineClear } from 'react-icons/ai';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import enrollWallpaper from '../../../wallpapers/allcourses-wallpaper2.jpg'

const TeacherEnroll = (props) => {
    const [unenrolled, setUnenrolled] = useState([]);
    const [enrolled, setEnrolled] = useState([]);

    const [pageEnrolled, setPageEnrolled] = useState(0);
    const [rowsPerPageEnrolled, setRowsPerPageEnrolled] = useState(10);
  
    const [pageUnenrolled, setPageUnenrolled] = useState(0);
    const [rowsPerPageUnenrolled, setRowsPerPageUnenrolled] = useState(10);

    const [keyWordEnrolled, setKeyWordEnrolled] = useState('');
    const [keyWordUnenrolled, setKeyWordUnenrolled] = useState('');

    const courseId = props.match.params.courseId;
    const [course, setCourse] = useState([]);

    const history = useHistory();


    useEffect(() => {
      getCourseById(courseId)
      .then(data => setCourse(data))
      .catch (error => setError(error.message));
    }, [courseId]);

    useEffect(() => {
        getAllUnenrolled(courseId)
          .then((data) =>setUnenrolled(data))
      }, []);

      useEffect(() => {
        getEnrolledStudents(courseId)
          .then((data) =>setEnrolled(data))
      }, []);


      const enroll = (courseId, data) => {
        teacherEnroll(courseId, data).then(r => {
           setUnenrolled(r.allUnenrolled)
           setEnrolled(r.allEnrolled)
        })
      }; 

      const unenroll = (courseId, data) => {
        teacherUnenroll(courseId, data).then(r => {
          setUnenrolled(r.unenrolled);
          setEnrolled(r.enrolled);
        })
      }

      const cancel = () => history.push('/courses');

      const columns = [
        { id: 'email', label: 'email', minWidth: 100 },
        { id: 'firstName', label: 'First name', minWidth: 100},
        { id: 'lastName', label: 'Last name', minWidth: 100 },
        { id: 'enroll', label: 'Enroll', minWidth: 100 },
      ];

      const createData = (email, firstName, lastName, enroll) =>{
        return { email, firstName, lastName, enroll };
      }

      const rowsEnrolled = enrolled
      .filter(item => 
        keyWordEnrolled === '' || (`${item.firstName} ${item.lastName}`).toLowerCase().includes(keyWordEnrolled.toLowerCase()))
      .map(item => 
        createData(item.email, item.firstName, item.lastName, <Button variant='outline-danger' onClick={() => unenroll(courseId, {email: item.email})}>Remove</Button>))

      const rowsUnenrolled = unenrolled
      .filter(item => 
        keyWordUnenrolled === '' || (`${item.firstName} ${item.lastName}`).toLowerCase().includes(keyWordUnenrolled.toLowerCase()))
      .map(item => 
        createData(item.email, item.firstName, item.lastName, <Button variant='outline-success' onClick={() => enroll(courseId, {email: item.email})}>Add</Button>))  

      const useStyles = makeStyles({
        root: {
          width: '45%',
          float: 'left',
         
        },
        container: {
          maxHeight: 260,
          height: 260,
        },
      });

  const classes = useStyles();

  const handleChangePageEnrolled = (_, newPage) => {
    setPageEnrolled(newPage);
  };

  const handleChangePageUnenrolled = (_, newPage) => {
    setPageUnenrolled(newPage);
  };

  const handleChangeRowsPerPageEnrolled = (event) => {
    setRowsPerPageEnrolled(+event.target.value);
    setPageEnrolled(0);
  };

  const handleChangeRowsPerPageUnenrolled = (event) => {
    setRowsPerPageUnenrolled(+event.target.value);
    setPageUnenrolled(0);
  };

    return (
      <div 
        style={{
        backgroundImage: `url(${enrollWallpaper})`,
        position: "absolute",
        width: "100%",
        height: "100vh",
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
      >
        <br />
        <div className='enroll-course-title'>{course.title}</div>
        <div id='section-back-btn'>
          <Button variant='success' style={{width: '200px'}} onClick={cancel}>Back</Button>
        </div>

        <div className='tables-container'>
          <Paper className={classes.root}>
            <div className='enrolled-text'>Enrolled</div>
            <div className="search-enrolled">
              <input
                className='enroll-search-bar'
                type="text" 
                value={keyWordEnrolled} 
                onChange={e => setKeyWordEnrolled(e.target.value)}
                placeholder="Student name here..."
              />
              <Tooltip title='Clear'>
                <Button variant='info' id='enroll-clear-btn' onClick={() => setKeyWordEnrolled('')}><AiOutlineClear /></Button>
              </Tooltip>
            </div>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                  ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rowsEnrolled
                  .slice(
                    pageEnrolled * rowsPerPageEnrolled,
                    pageEnrolled * rowsPerPageEnrolled + rowsPerPageEnrolled
                  )
                  .map((row) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.email}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={rowsEnrolled.length}
              rowsPerPage={rowsPerPageEnrolled}
              page={pageEnrolled}
              onPageChange={handleChangePageEnrolled}
              onRowsPerPageChange={handleChangeRowsPerPageEnrolled}
            />
          </Paper>

          <Paper className={classes.root}>
            <div className='unenrolled-text'>Available students</div>
            <div className="search-unenrolled">
              <input
                className='unenroll-search-bar'
                type="text" 
                value={keyWordUnenrolled} 
                onChange={e => setKeyWordUnenrolled(e.target.value)}
                placeholder="Student name here..."
              />
              <Tooltip title='Clear'>
                <Button variant='info' id='unenroll-clear-btn' onClick={() => setKeyWordUnenrolled('')}><AiOutlineClear /></Button>
              </Tooltip>
            </div>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                  ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rowsUnenrolled
                  .slice(
                    pageUnenrolled * rowsPerPageUnenrolled,
                    pageUnenrolled * rowsPerPageUnenrolled + rowsPerPageUnenrolled
                  )
                  .map((row) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.email}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={rowsUnenrolled.length}
              rowsPerPage={rowsPerPageUnenrolled}
              page={pageUnenrolled}
              onPageChange={handleChangePageUnenrolled}
              onRowsPerPageChange={handleChangeRowsPerPageUnenrolled}
            />
          </Paper>
        </div>
      </div>             
    );
}

export default TeacherEnroll;
