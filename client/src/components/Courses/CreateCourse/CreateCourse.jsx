import React, { useState } from "react";
import { useHistory } from "react-router";
import { createCourse } from "../../../requests/courses";
import { Button, Form } from "react-bootstrap";
import './CreateCourse.css'
import createCourseWallpaper from '../../../wallpapers/allcourses-wallpaper2.jpg'

const CreateCourse = () => {
    const [course, setCourse] = useState({
        title: '',
        description: '',
    });
    const [error, setError] = useState(null);

    const history = useHistory()

    const submitCourse = (data) => {
      if((data.title.length < 2 && typeof data.title !== 'string') 
      || (data.description.length < 10 && typeof data.description.length !== 'string')) {
        return alert('Invalid input');
      }

        createCourse(data)
        .then(newCourse => 
            setCourse(newCourse))
        .catch (error => setError(error.message))
        .then(() => history.goBack());
    }

       const cancel = () => history.goBack();

    if (error) {
        return (
          <h4>
            <i>An error has occured: </i>
            {error}
          </h4>
        );
    }

    return(
      <div>
        <div
          style={{
          backgroundImage: `url(${createCourseWallpaper})`,
          position: "absolute",
          width: "100%",
          height: "100vh",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
        >
          <div id='new-course-text'>Add new course</div>
          <div className="create-course-form">
            <Form>
              <Form.Group className="mb-10" controlId="ControlInput2">
                <Form.Control
                  style={{
                border: 'solid rgb(15, 120, 75) 1px',
                width: '500px',
                borderRadius: '5px',
                boxShadow: '5px 5px 20px rgb(23, 88, 32)',
                marginBottom: '10px',
              }}
                  type="text"
                  placeholder="Title"
                  value={course.title}
                  onChange={(e) => setCourse({ ...course, title: e.target.value })}
                />
              </Form.Group>
              <div style={{color: 'white', textShadow: '1px 1px 5px black'}}>At least 3 characters</div>
              <br />
              <Form.Group className="mb-10" controlId="ControlInput2">
                <Form.Control
                  style={{
                  border: 'solid rgb(15, 120, 75) 1px',
                  borderRadius: '5px',
                  boxShadow: '5px 5px 20px rgb(23, 88, 32)',
                  marginBottom: '10px',
                  minHeight: '200px'
              }}
                  as="textarea"
                  placeholder="Description"
                  value={course.description}
                  onChange={(e) => setCourse({ ...course, description: e.target.value })}
                />
                <div style={{color: 'white', textShadow: '1px 1px 5px black'}}>At least 10 characters</div>
                <br />
              </Form.Group>
              <div className='create-course-btns'>
                <Button variant='success' style={{marginRight: '5px'}} onClick={e => submitCourse(course)}>Create</Button>
                <Button variant='danger' style={{marginLeft: '5px'}} onClick={cancel}>Cancel</Button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    )
}

export default CreateCourse;
