import React, { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Button, Form } from "react-bootstrap";
import { getAllTeachers } from "../../../requests/users";
import SingleTeacher from "../SingleTeacher/SingleTeacher";
import AuthContext from "../../../providers/AuthContext";
import './AllTeachers.css';
import { Tooltip } from "@material-ui/core";
import { AiOutlineClear } from "react-icons/ai";

const AllTeachers = () => {

    const { user } = useContext(AuthContext);

    const [teachers, setTeachers] = useState([])
    const [keyWord, setKeyWord] = useState('');
    const [error, setError] = useState(null);

    useEffect(() =>{
        getAllTeachers()
        .then(data => setTeachers(data))
        .catch (error => setError(error.message));
    }, []);

    if (error) {
        return (
          <h4>
            <i>An error has occured: </i>
            {error}
          </h4>
        );
    }

    return (  
      <div className="teachers-wallpaper">
        <Form>
          <div>
            <Form.Group className="mb-3" controlId="ControlInput2">
              <div className="all-teachers-label"><Form.Label>Our Teachers</Form.Label></div>
            </Form.Group>
            <div className="search-input">
              <input type="text" className='search-input-style' value={keyWord} onChange={e => setKeyWord(e.target.value)} />
              <Tooltip title='Clear'>
                <Button variant='info' onClick={() => setKeyWord('')} id='clear-btn-style'><AiOutlineClear /></Button>
              </Tooltip>
              <div style={{fontSize: '25px', marginLeft: '25px', fontFamily: 'Kaisei HarunoUmi'}}><Link style={{color: 'white'}} to='/users'>Our Students</Link></div>
            </div>
            <Form.Group>, 
              {teachers
                    .filter(t => keyWord === '' || (`${t.firstName} ${t.lastName}`).toLowerCase().includes(keyWord.toLowerCase()))
                    .map(t => user? <Link to={{pathname: `./${t.id}`, state: {teachers: teachers.filter(item => item.id === t.id)}}} key={t.id}><SingleTeacher key={t.id} teacher={t} /></Link>
                    : <SingleTeacher key={t.id} teacher={t} />)}
            </Form.Group>
          </div>
        </Form>
      </div>
    );
};

export default AllTeachers;

