import React, {useContext} from "react";
import { Card } from "react-bootstrap";
import AuthContext from "../../../providers/AuthContext";
import './SingleStudent.css';

const SingleStudent = ({ student }) => {

    const { user } = useContext(AuthContext);

    return (
      <div className="single-student">
        <Card style={{width: '100%', marginBottom: '15px'}} className="student-card">
          <Card.Img
            variant="top"
            src="../students.jpg"
            height="200"
          />
          <Card.Body style={{height: '100px'}}>
            <h4 className="student-name">
              {user &&  <span>{`${student.firstName} ${student.lastName}`}</span>}
            </h4>
          </Card.Body>
        </Card>
      </div>
    );
};

export default SingleStudent;
