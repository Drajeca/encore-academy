import React, {useContext} from "react";
import { Card } from "react-bootstrap";
import AuthContext from "../../../providers/AuthContext";
import './SingleTeacher.css';

const SingleTeacher = ({ teacher }) => {

    const { user } = useContext(AuthContext);

    return (

      <div className="single-teacher">
        <Card style={{width: '100%'}} className="teacher-card">
          <Card.Img
            variant="top"
            src="../teachers2.jpg"
            height="200"
          />
          <Card.Body>
            <h4 className="teacher-name">
              {user &&  <span>{`${teacher.firstName} ${teacher.lastName}`}</span>}
            </h4>
          </Card.Body>
        </Card>
      </div>
  
    );
};

export default SingleTeacher;
