import React, { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Button, Form } from "react-bootstrap";
import { getAllStudents } from "../../../requests/users";
import SingleStudent from "../SingleStudent/SingleStudent";
import AuthContext from "../../../providers/AuthContext";
import './AllStudents.css';
import { Tooltip } from "@material-ui/core";
import { AiOutlineClear } from "react-icons/ai";

const AllStudents = () => {

    const { user } = useContext(AuthContext);

    const [students, setStudents] = useState([])
    const [keyWord, setKeyWord] = useState('');
    const [error, setError] = useState(null);

    useEffect(() =>{
        getAllStudents()
        .then(data => setStudents(data))
        .catch (error => setError(error.message));
    }, []);

    if (error) {
        return (
          <h4>
            <i>An error has occured: </i>
            {error}
          </h4>
        );
    }

    return (  
      <div className="students-wallpaper"> 
        <Form>
          <div>
            <Form.Group className="mb-3" controlId="ControlInput2">
              <div className="all-students-label"><Form.Label>Our Students</Form.Label></div>
            </Form.Group>
            <div className="search-input">
              <input type="text" className='search-input-style' value={keyWord} onChange={e => setKeyWord(e.target.value)} />
              <Tooltip title='Clear'>
                <Button variant='info' onClick={() => setKeyWord('')} id='clear-btn-style'><AiOutlineClear /></Button>
              </Tooltip>
              <div style={{fontSize: '25px', marginLeft: '25px', fontFamily: 'Kaisei HarunoUmi'}}><Link style={{color: 'white'}} to='/users/teachers'>Our Teachers</Link></div>
            </div>
            <Form.Group>
              {students
                    .filter(s => user? s.id !== user.sub : s)
                    .filter(s => keyWord === '' || (`${s.firstName} ${s.lastName}`).toLowerCase().includes(keyWord.toLowerCase()))
                    .map(s => user? <Link to={`./users/${s.id}`} key={s.id}><SingleStudent key={s.id} student={s} /></Link>
                    : <SingleStudent key={s.id} student={s} />)}
            </Form.Group>
          </div>
        </Form>
      </div>
    );
};

export default AllStudents;
