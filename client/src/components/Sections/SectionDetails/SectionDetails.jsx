import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { getSection } from "../../../requests/sections";
import Parser from 'html-react-parser';
import { Button, Spinner } from "react-bootstrap";
import { validId } from "../../../common/helpers";
import './SectionDetails.css'

const SectionDetails = (props) => {
    const [error, setError] = useState('');
    const [section, setSection] = useState( 
    { title: '',
    content: '',
    availableFrom: '',
    orderPosition: ''  
    });

    const [loading, setLoading] = useState(false);
    
    const history = useHistory();
    const { courseId, sectionId } = props.match.params;
    
    const validIds = validId([+courseId, +sectionId])
    const goBack = () => history.goBack();

    if(!validIds) return <div>Content not found</div>


  useEffect(() => {
        setLoading(true);
        getSection(courseId, sectionId)
          .then((data) => setSection(data))
          .catch((error) => setError(error.message))
          .finally(() => setLoading(false))
      }, [courseId, sectionId]);

    return (
      <div className='section-details-wallpaper'>
        <div className='back-btn'><Button variant='success' style={{width: '200px'}} onClick={goBack}>Back</Button></div>
        {loading &&
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>}
        <br />
        <div className='section-title'>{section.title}</div><br />
        <div className='section-content'>{Parser(section.content)}</div>
      </div>
    )
}

  export default SectionDetails;
