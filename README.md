# Final team project

## Project Name & Pitch

MYsic compoSITE

An e-Learning Platform, built with React, Express, JavaScript and CSS.

## Project Status

This project is currently under development.
1. Guests: 
- can visit the home page
- can register to the site and become a student
- can visit courses page and search by title or description keywords
2. Students:
- can visit the home page, login and logout
- can visit courses page and search by title or description keywords
- can enroll to or un-enroll from a public course and explore non-restricted course sections
- can visit the students and teachers pages
- can view all users' details including enrollment/course owner status
- can edit or delete own profile
3. Teachers:
- have all students' privileges
- can create, edit and delete public and private courses
- can create, edit, reorder, restrict and delete course sections
- can enroll and un-enroll students

## Project Screen Shot(s)

#### Example:

<img src='client/public/screenshots/guests-all-courses.png'>
<br /><br />
<img src='client/public/screenshots/students-section-details.JPG'>
<br /><br />
<img src='client/public/screenshots/create-edit-section.JPG'>
<br /><br />
<img src='client/public/screenshots/reorder-sections.jpg'>
<br /><br />
<img src='client/public/screenshots/enroll-students.JPG'>
<br /><br />

## Installation and Setup Instructions

1. Database
  - Import database from database folder into My SQL workbench.
  - Don't forget to change your credentials in the config.js file, located in the server folder.

2. Server-side
  - Navigate to the root level aka /server and install node modules.
  - You can start the server by executing the following command: npm start / npm run start:dev
  - Server listens on port 5555.

3. Client-side
  - Navigate to the root level aka /client and install node modules.
  - You can start the client by executing the following command: npm start
  - You can access the application by entering localhost:3000.

4. Teacher login
  - If you want to try a teacher profile you can use: **username: teacher@gmail.com, password: root1**
