CREATE DATABASE  IF NOT EXISTS `poodle_db` /*!40100 DEFAULT CHARACTER SET utf8mb3 */;
USE `poodle_db`;
-- MariaDB dump 10.19  Distrib 10.6.2-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: poodle_db
-- ------------------------------------------------------
-- Server version	10.6.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext NOT NULL,
  `description` longtext NOT NULL,
  `isPublic` tinyint(2) NOT NULL DEFAULT 0,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `userId` int(11) NOT NULL,
  `isDeleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_courses_users_idx` (`userId`),
  CONSTRAINT `fk_courses_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Electric Guitar - Beginners','We\'ve been teaching over 25 years now and realized that when we taught this free beginners course, 98-99% of the students came back and signed up. Here it is both for teachers and for new students.',1,'2021-07-25 16:31:38',1,0),(2,'7 Days To Learning Piano ','There\'s never been a better time to start learning to play the piano!',0,'2021-07-28 18:43:26',1,0),(3,'Intermediate Advanced Violin Lessons','Improve your violin skills with several practical exercises. ',1,'2021-07-31 05:51:42',1,0),(4,'How to play drums','In this video, I wanted to give all the brand new drummers something to practice. So even if you don\'t have a drum-set, this lesson will test your rhythm skills and hopefully get you started playing your first song.',0,'2021-08-02 18:58:52',1,0),(5,'Rock Organ','The Hammond organ is one of the definitive sounds of the rock keys player. Here\'s everything beginners need to know about it.',1,'2021-08-04 22:02:08',1,0),(6,'Bass Workout For All Levels','You’re in for a fun time this lesson. We’ve got a killer bass workout to show you that’s pretty epic! It’s just as useful for rank beginning bass players as it is for seasoned pros - there’s a version of this exercise that will work for everyone.',1,'2021-08-05 19:37:09',1,0),(7,'How To Sing Any Song - Voice Lessons ','Einstein\'s definition of insanity is \"trying the same experiment over and over and expecting a different result.\" You should see results even if they are small.',0,'2021-08-08 12:22:09',3,0),(8,'Bulgarian Gaida for beginners','Start with getting to know one of the most prominent bulgarian national musical instrument.',1,'2021-08-10 09:44:46',3,0),(9,'Intermediate Flute Exercises','This course covers Flute sound development, scales, technique, and even where to find etudes! We also give you a basic outline of how to run your daily practice as well. Enjoy!',1,'2021-08-15 03:17:24',4,0),(10,'Simple Concepts - Beginner\'s Double Bass','Sharing some simple concepts to help you get a great sound on the double bass. Topics covered include: creating a nice sound, right-hand technique, left-hand technique, and stopping the string buzzing on the fingerboard. ',1,'2021-08-19 19:31:59',4,0),(11,'Classical Pieces In Rock Shred Guitar','Neoclassical guitar course for professionals. Reach the limit of your technical skills!',0,'2021-08-22 11:36:56',5,0),(12,'Beginner Saxophone','If you’re a beginner on the saxophone and are looking for the best place to start, this is the “how to” course for you!',0,'2021-08-25 15:32:33',3,0),(13,'Modern Drums Classical Music Pro','Learn more full “classical” pieces with modern drums.',0,'2021-08-26 09:49:03',3,0);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrolled`
--

DROP TABLE IF EXISTS `enrolled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enrolled` (
  `courseId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`courseId`,`userId`),
  KEY `fk_courses_has_users_users1_idx` (`userId`),
  KEY `fk_courses_has_users_courses1_idx` (`courseId`),
  CONSTRAINT `fk_courses_has_users_courses1` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_users_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolled`
--

LOCK TABLES `enrolled` WRITE;
/*!40000 ALTER TABLE `enrolled` DISABLE KEYS */;
INSERT INTO `enrolled` VALUES (1,2),(1,9),(1,19),(1,26),(1,27),(2,7),(2,8),(2,14),(2,15),(2,17),(2,29),(3,11),(3,16),(3,25),(4,16),(4,22),(5,17),(5,21),(5,24),(6,2),(6,13),(6,15),(6,27),(7,11),(7,14),(7,22),(7,23),(8,2),(8,7),(8,8),(8,9),(8,10),(8,11),(8,20),(9,31),(10,10),(10,14),(10,15),(10,24),(10,25),(10,26),(10,28),(10,30),(11,9),(11,16),(11,18),(11,20),(11,26),(11,29),(12,7),(12,13),(12,16),(12,27),(12,30),(13,7),(13,24);
/*!40000 ALTER TABLE `enrolled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Teacher'),(2,'Student');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext NOT NULL,
  `content` mediumtext NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `availableFrom` datetime DEFAULT NULL,
  `courseId` int(11) NOT NULL,
  `orderPosition` int(11) DEFAULT NULL,
  `isDeleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_sections_courses1_idx` (`courseId`),
  CONSTRAINT `fk_sections_courses1` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'Electric Guitar Introduction','<iframe src=\"https://www.youtube.com/embed/w4a2ge9N31E\" height=\"315\" width=\"560\"></iframe>','2021-08-25 16:32:06','2021-08-01 00:00:00',1,1,0),(2,'Electric Guitar Lesson 1','<iframe src=\"https://www.youtube.com/embed/l_NoccF3RaI\" height=\"315\" width=\"560\"></iframe>','2021-08-25 16:32:48','2021-08-02 00:00:00',1,2,0),(3,'Electric Guitar Lesson 2','<iframe src=\"https://www.youtube.com/embed/5LFsTsscc1I\" height=\"315\" width=\"560\"></iframe>','2021-08-25 16:33:44','2021-08-03 00:00:00',1,3,0),(4,'Electric Guitar Lesson 3','<iframe src=\"https://www.youtube.com/embed/FxE2Zp06O4g\" height=\"315\" width=\"560\"></iframe>','2021-08-25 16:34:22','2021-09-04 00:00:00',1,4,0),(5,'Beginner Lesson','<iframe src=\"https://www.youtube.com/embed/bzNKQ2FkEJI\" height=\"315\" width=\"560\"></iframe>','2021-08-25 18:44:57','2021-08-01 00:00:00',2,1,0),(6,'Practice for Every Day','<iframe src=\"https://www.youtube.com/embed/aD47CHWgdh8\" height=\"315\" width=\"560\"></iframe>','2021-08-25 18:46:17','2021-08-10 00:00:00',2,2,0),(7,'Violin Lesson 1','<iframe src=\"https://www.youtube.com/embed/9SyHNPHWehk\" height=\"315\" width=\"560\"></iframe>','2021-08-25 18:52:38','2021-08-01 00:00:00',3,1,0),(8,'Violin Lesson 3','<iframe src=\"https://www.youtube.com/embed/q-hVLUe-hQw\" height=\"315\" width=\"560\"></iframe>','2021-08-25 18:53:33','2021-08-20 00:00:00',3,2,0),(9,'Violin Lesson 2','<iframe src=\"https://www.youtube.com/embed/nhAr4ll5QC8\" height=\"315\" width=\"560\"></iframe>','2021-08-25 18:55:42','2021-08-10 00:00:00',3,3,0),(10,'Your Very First Drum Lesson','<iframe src=\"https://www.youtube.com/embed/et9hU7QMDYU\" height=\"315\" width=\"560\"></iframe>','2021-08-25 18:59:31','2021-08-01 00:00:00',4,1,0),(11,'Beginner Drum Lesson','<iframe src=\"https://www.youtube.com/embed/ondTlJEmPYA\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:00:21','2021-08-25 00:00:00',4,2,0),(12,'5 Beginner Drumming Techniques ','<iframe src=\"https://www.youtube.com/embed/0Krnkf865SI\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:00:54','2021-08-10 00:00:00',4,3,0),(13,'Piano Lesson','<iframe src=\"https://www.youtube.com/embed/eSQfT6atP_E\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:02:36','2021-08-01 00:00:00',5,1,0),(14,'Play Rock \'n Roll Piano Like It\'s the 50\'s','<iframe src=\"https://www.youtube.com/embed/7mMQQDcVEDM\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:05:04','2021-08-25 00:00:00',5,2,0),(15,'Beginner, Intermediate AND Advanced Versions','<iframe src=\"https://www.youtube.com/embed/-P--lY3xwtU\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:07:37','2021-08-18 00:00:00',6,1,0),(16,'Ken Tamplin Vocal Lesson','<iframe src=\"https://www.youtube.com/embed/ZATunybJm_4\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:10:48','2021-08-01 00:00:00',7,1,0),(17,'Want a Smooth Voice? Do This:','<iframe src=\"https://www.youtube.com/embed/pOYDSo-ZFBg\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:11:55','2021-08-25 00:00:00',7,2,0),(18,'Gaida Beginners Lesson','<iframe src=\"https://www.youtube.com/embed/CuqCFKTf2QY\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:16:24','2021-08-12 00:00:00',8,1,0),(19,'Flute Exercises for Daily Practice','<iframe src=\"https://www.youtube.com/embed/OlsNNu4VXF0\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:29:17','2021-08-25 00:00:00',9,1,0),(20,'Flute Lessons: Long Tones','<iframe src=\"https://www.youtube.com/embed/mGC42YSWHks\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:30:07','2021-08-26 00:00:00',9,2,0),(21,'Beginner\'s Double Bass Lesson ','<iframe src=\"https://www.youtube.com/embed/_A0BcAnpbXU\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:33:49','2021-08-02 00:00:00',10,1,0),(22,'Basics: Double Bass Technique for Beginners','<iframe src=\"https://www.youtube.com/embed/-HQ6_G8kPxI\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:34:22','2021-08-13 00:00:00',10,2,0),(23,'Mozart / Paganini / Bach','<iframe src=\"https://www.youtube.com/embed/1Uj6lhDFdbg\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:37:38','2021-08-26 00:00:00',11,1,0),(24,'Turkish March ','<iframe src=\"https://www.youtube.com/embed/9NjBWqV68xo\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:38:55','2021-08-27 00:00:00',11,2,0),(25,'Flight Of The Bumblebee','<iframe src=\"https://www.youtube.com/embed/ijZuXkzXA38\" height=\"315\" width=\"560\"></iframe>','2021-08-25 19:40:14','2021-08-28 00:00:00',11,3,0),(26,'Vivaldi - The Four Seasons - Winter','<iframe src=\"https://www.youtube.com/embed/RqhYzIx19E8\" height=\"315\" width=\"560\"></iframe>','2021-08-26 10:24:20','2021-08-01 00:00:00',13,1,0),(27,'Swan Lake - Tchaikovsky','<iframe src=\"https://www.youtube.com/embed/5TxMjEKT2BM\" height=\"315\" width=\"560\"></iframe>','2021-08-26 10:25:00','2021-08-02 00:00:00',13,2,0),(28,'Beethoven - Moonlight Sonata','<iframe src=\"https://www.youtube.com/embed/vL1k7GKa1jc\" height=\"315\" width=\"560\"></iframe>','2021-08-26 10:28:05','2021-08-03 00:00:00',13,3,0);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `roleId` int(11) NOT NULL,
  `isDeleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_roles_idx` (`roleId`),
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'drajo@gmail.com','Dragomir','Krastev','$2b$10$mD6oFrgFKFtVsRmEgQSPJuN/bKZWJOMMxIcpAWR9GKObgIMCIsdme','2021-07-25 16:28:15',1,0),(2,'hristo@abv.bg','Hristo','Peynov','$2b$10$XWgEY1FvDZH/lZ0uZLKyh.QiI4KtAjQjK7l20kmUs6mNRnZKl.yQC','2021-07-27 16:29:06',2,0),(3,'nadezhda.h@hotmail.com','Nadezhda','Hristova','$2b$10$kG3CrcC4EBKln.hwM6XHaOrP00yvOZvtPTHJgHd0kjduEq5v0ES6i','2021-07-29 16:37:49',1,0),(4,'stoyan.p@yahoo.com','Stoyan','Peshev','$2b$10$I2ULKzcJSi.RpKszNvjgzOqzmk0z6LqFMET6p9oDp3gp0RF8w7L7m','2021-07-31 16:38:44',1,0),(5,'edward.e@hotmail.co.uk','Edo','Evlogiev','$2b$10$4mCvW0SbWxzGh0dKIQSOYO6Na8PZpV0b2GrEMIoyE1.Fofe7thY3S','2021-08-01 16:41:30',1,0),(6,'nadya.a@orange.fr','Nadya','Atanasova','$2b$10$56TRH.xf0GTLQFcXiOgd3uPy274h5JWedG90marm2N6.BF/1w2K/.','2021-08-02 16:42:12',1,0),(7,'angel.a@abv.bg','Angel','Angelov','$2b$10$PvaBRGmFDJUL7IHBv9AAhuuyJTuuJ.gqrEifQXNwocL3nJFzalizq','2021-08-03 16:45:29',2,0),(8,'boris.b@abv.bg','Boris','Borisov','$2b$10$tdHMzn1GV21AMYySyjJype.wK7AD/28ZVrykKHOiqQhTk8hJG.w7i','2021-08-04 16:45:53',2,0),(9,'georgi.g@abv.bg','Georgi','Georgiev','$2b$10$bo9Foi70VSPgwu2wfnW.2OVlk2/pXVZSw.A67lmjtM8DH4ggUyrUy','2021-08-05 16:48:08',2,0),(10,'teodora.t@abv.bg','Teodora','Todorova','$2b$10$yLJ9dH8LFCW2dpSixy4.jeHm5J2kaW3a4knE9s8MNIxoRDnqU.1g6','2021-08-06 16:48:48',2,0),(11,'desislava.d@abv.bg','Desislava','Desislavova','$2b$10$Wq4M8.Po46pXWefooilSk.6gToQKaKCOVBtb06u6npPGv18pCjMim','2021-08-07 16:51:23',2,0),(12,'marie.m@gmx.de','Marie','Muller','$2b$10$I1O9Rip9YvZc/TmLsDOfXePXjOwJXxp1sMHmbywIHqFLLXji2UHhq','2021-08-08 16:52:43',2,0),(13,'james.j@aol.com','James','Jones','$2b$10$Tu5fl.EQZFERkPKac4SnkOXw1HLYC6.1OQDREHIoxEjW59o/U8gey','2021-08-09 16:53:48',2,0),(14,'sofia.s@yandex.ru','Sofia','Smirnova','$2b$10$QryxefXAFflybAqiZSfb0.AjVh2GZqYqM5j472pAlxVw4DQkrxrEa','2021-08-10 16:55:02',2,0),(15,'yichen.y@msn.com.cn','Yichen','Yang','$2b$10$lnCljuHtaJeewLVaAGyh2ORMNDowbhTlAGgbDxBy4ubOWe4TBN/om','2021-08-11 17:00:53',2,0),(16,'louise.l@wanadoo.fr','Louise','Laurent','$2b$10$m8X2I./NBWjLYqXmn6Rjx.l5EBc/EPd7UnrxJBRZxB39BBNgoyku.','2021-08-12 17:03:04',2,0),(17,'ricardo.r@libero.it','Riccardo','Rossi','$2b$10$uB1cI7KeBuJDqk/DfB3eWOpZwluWrDB0cP8.uUd6o3oXZiFCs30gG','2021-08-13 17:04:49',2,0),(18,'maria.m@hotmail.es','Maria','Martinez','$2b$10$wnCtMYEsL3tPCx5l8U3n2.ADWISyT.K2/xHvPF4Z9LubRuZmw0Kl.','2021-08-14 17:05:47',2,0),(19,'milica.m@org.rs','Milica','Markovic','$2b$10$lA6mfMN7n1yH0yBPJnPVoOBNUBGBIvQW.KiCkr5xInsITODvfqKpi','2021-08-15 17:08:13',2,0),(20,'panagiotis.p@edu.gr','Panagiotis','Papadopoulos','$2b$10$WqWnZY1wwArv47VFjATf3.p7DiQWWGQiCq9y6n3VyvQ0Mc3eFlATG','2021-08-16 17:10:17',2,0),(21,'dumitru.d@gov.ro','Dumitru','Dumitrescu','$2b$10$oGxk2Z7wSPWK8Y/qpMtOzOlazNjRsSCNge8Xd2SQwfnhmPfUpVsyG','2021-08-17 17:13:27',2,0),(22,'krzyztof.k@art.pl','Krzyztof','Kowalski','$2b$10$kEfeI3o4qLpMwArnhCeWXea.FkKqUzSct8cYqCInA7yczaVKDhcyK','2021-08-18 17:16:14',2,0),(23,'gudrun.g@info.is','Gudrun','Gudmundsdottir','$2b$10$pEoKxWyr7YuHGwuytsp9p.yuReaDJ3Rt98XiNajwcv7Oef4pUhM2C','2021-08-19 17:18:37',2,0),(24,'samuel.s@uol.com.br','Samuel','Santos','$2b$10$UjiwLxataP9EPZrXMgi1hum7VWP9d9D5RPNGfBbaizv8FN5aw./uW','2021-08-20 17:22:49',2,0),(25,'william.w@live.com.au','William','Williamson','$2b$10$eQeXuPdXrAluWlvw0gIjUOrfxD4Tl0mg8iuHJ65kQ9zftCGIvXjl.','2021-08-21 17:25:42',2,0),(26,'singh.s@res.is','Singh','Singh','$2b$10$8rYE71WOHI7z5EhoQZOTw.si/tGnapneO3QEE9X9S5W48RAY7.mqm','2021-08-22 17:29:38',2,0),(27,'ashkam.a@net.ir','Ashkam','Ahmadi','$2b$10$RS.pzI88xZ8owbadiTJ6TufR3BXmU4BfR0EvoehJuF.k9p.xuHbQK','2021-08-23 17:30:40',2,0),(28,'mohamed.m@daily.ac.eg','Mohamed','Mohamed','$2b$10$3YfuNzOVgESz0Q0SIn7brOAOzQCe/GmcDLfFIHPEXnPcw94SD7Exa','2021-08-24 17:32:08',2,0),(29,'mozes.m@sport.co.ug','Mozes','Mbabazi','$2b$10$CAnWpONFxmXi3rrBSpkyWOLyZAqZhBfNm.EJdz3xG14eyvz3xjdKq','2021-08-25 17:33:20',2,0),(30,'tamang.t@music.bt','Tamang','Tshering','$2b$10$Yl5UeBaKoHjA/D57VwKoNe6IHWr2aXbjmlbxXFg/hogaojAzcSna.','2021-08-26 17:35:09',2,0),(31,'bilguun.b@songs.edu.mn','Bilguun','Batbayar','$2b$10$wtiegM7rdeRqyBfmhSyklupc6oI7Xemy21/EvhTeeqYNQdXIu/Xxq','2021-08-27 21:00:26',2,0),(32,'teacher@gmail.com','Teacher','Teacher','$2b$10$6sxbTkX1fUthReSqXofqZesziNIc2BZ7HkBh4Jk6IMUr1QAdNTflG','2021-09-02 10:09:24',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-02 11:48:10
