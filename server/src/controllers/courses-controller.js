import express from 'express';
import { STUDENT_ROLE, TEACHER_ROLE } from '../../config.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import coursesData from '../data/courses-data.js';
import coursesService from '../services/courses-service.js';
import errors from '../services/errors.js';
import bodyValidator from '../validators/validate-body.js';
import createCourseSchema from '../validators/create-course-validator.js';
import updateCourseSchema from '../validators/update-course-validator.js';

const coursesController = express.Router();

coursesController

// get all courses
.get('/', async (req, res) => {

    const courses = await coursesService.getAllCourses(coursesData);

    res.status(200).send(courses);
})

// get course by id
.get('/:id',
authMiddleware,
roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]),
async (req, res) => {
    const { id } = req.params;

    const { error, course } = await coursesService.getCourseById(coursesData)(+id);

    if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Course not found!' });
    } else {
        res.status(200).send(course);
    }
})

// create course
.post('/', 
authMiddleware, 
roleMiddleware([TEACHER_ROLE]), 
bodyValidator(createCourseSchema),
async (req, res) => {
    const createData = req.body;
    createData.userId = req.user.email;

    const { error, course } = await coursesService.createCourse(coursesData)(createData);
    if (error === errors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Title is not unique!' });
    } else {
        res.status(201).send(course);
    }
})

// update course by id
.put('/:id',
authMiddleware, 
roleMiddleware([TEACHER_ROLE]), 
bodyValidator(updateCourseSchema),
async (req, res) => {
    const id = +req.params.id;
    const updateData = req.body;
   

    const { error, course } = await coursesService.updateCourse(coursesData)(id, updateData);

    if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Course not found!' });
    } else if (error === errors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Title not available' });
    } else {
        res.status(200).send(course);
    }
})

// delete course by id
.delete('/:id', 
authMiddleware, 
roleMiddleware([TEACHER_ROLE]), 
async (req, res) => {
const { id } = req.params;
const { error, course } = await coursesService.deleteCourse(coursesData)(+id);

if (error === errors.RECORD_NOT_FOUND) {
    res.status(404).send({ message: 'Title not found!' });
} else {
    res.status(200).send({ message: `Course ${course.title} was successfully deleted` });
}
})

// student self-enroll in a course
.post('/:id/enroll',
    authMiddleware,
    roleMiddleware([STUDENT_ROLE]),
    async (req, res) => {
        const { id } = req.params;
        const userId = req.user.email;

        const { error, enroll } = await coursesService.enrollStudent(coursesData)(+id, userId);
        
        if(error === errors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'You are trying to enroll a student into a course that doesn\'t exist!' });
        }

        if(error === errors.DUPLICATE_RECORD) {
            return res.status(409).send({ message: 'You are already enrolled!'});
        }
            res.status(200).send(enroll);
    })

// get all enrolled students from a single course by courseId
.get('/:id/enroll',
    authMiddleware,
    roleMiddleware([TEACHER_ROLE, STUDENT_ROLE]),
    async (req, res) => {
        const { id } = req.params;

        const { error, enroll } = await coursesService.getAllEnrolledByCourse(coursesData)(+id);
        
        if(error === errors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'Missing course/no enrolled students' });
        }
            res.status(200).send(enroll);
    })

// teacher enroll student    
.post('/:id/enroll/teacher',
    authMiddleware,
    roleMiddleware([TEACHER_ROLE]),
    async (req, res) => {
        const { id } = req.params;
        const userId = req.body.email;

        const { error, enroll } = await coursesService.enrollStudent(coursesData)(+id, userId);
        
        if(error === errors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'You are trying to enroll a student into a course that doesn\'t exist!' });
        }

        if(error === errors.DUPLICATE_RECORD) {
            return res.status(409).send({ message: 'You have already enrolled this student!'});
        }
            res.status(200).send(enroll);
    })
    
 //student self-unenroll   
.delete('/:id/unenroll',
    authMiddleware,
    roleMiddleware([STUDENT_ROLE]),

    async (req, res) => {
        const { id } = req.params;
        const userId = req.user.email;

        const { error, enroll } = await coursesService.unenrollStudent(coursesData)(+id, userId);

        if(error === errors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'You are trying to unenroll a student into a course that doesn\'t exist!' });
        }

        res.status(200).send(enroll);
    })

    //teacher unenroll student
.delete('/:id/unenroll/teacher',
    authMiddleware,
    roleMiddleware([TEACHER_ROLE]),


    async (req, res) => {
        const { id } = req.params;
        const userId = req.body.email;

        const { error, enroll } = await coursesService.unenrollStudentByTeacher(coursesData)(+id, userId);

        if(error === errors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'You are trying to unenroll a student into a course that doesn\'t exist!' });
        }

        res.status(200).send(enroll);
    })

.get('/:courseId/unenrolled',
    authMiddleware,
    roleMiddleware([TEACHER_ROLE]),
    async (req, res) => {
        const courseId = +req.params.courseId;

        const { error, user } = await coursesService.unenrolledByCourse(coursesData)(courseId);

        if(error === errors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Course not found' });
        } else {
            res.status(200).send(user);
        }
})

  // get all visible courses by sudents
.get('/visible/students', authMiddleware, roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]), async (req, res) => {
    const id = req.user.id;

    const { allVisible } = await coursesService.getAllVisibleCourses(coursesData)(id);

    res.status(200).send(allVisible);
});

export default coursesController;
