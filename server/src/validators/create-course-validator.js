export default {
    title: (value) => typeof value === 'string' && value.length > 2 ,
    description: (value) => typeof value === 'string' && value.length > 10,
  };
  