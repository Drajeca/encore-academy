export default {
    title: (value) => typeof value === 'string' && value.length > 2 ,
    content: (value) => typeof value === 'string' && value.length > 10,
  };