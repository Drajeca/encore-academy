import { STUDENT_ROLE } from '../../config.js';
import errors from './errors.js';
import bcrypt from 'bcrypt';

const signInUser = (usersData) => {
  return async (email, password) => {
    const user = await usersData.getWithRole(email);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: errors.INVALID_SIGNIN,
        user: null,
      };
    }
    return {
      error: null,
      user: user,
    };
  };
};

const getStudents = usersData => {
  return async (filter) => {
      return filter
          ? await usersData.searchBy('firstName', filter)
          : await usersData.getAllStudents();
  };
};

const getTeachers = usersData => {
  return async (filter) => {
      return filter
          ? await usersData.searchBy('firstName', filter)
          : await usersData.getAllTeachers();
  };
};

const getUserById = usersData => {
  return async (id) => {
      const user = await usersData.getBy('u.id', id);

      if (!user) {
          return {
              error: errors.RECORD_NOT_FOUND,
              user: null,
          };
      }

      return { error: null, user: user };
  };
};

const createUser = (usersData) => {
  return async (userCreate) => {
    const { email, firstName, lastName, password } = userCreate;
    const existingUser = await usersData.getBy('email', email);
    if (existingUser) {
      return {
        error: errors.DUPLICATE_RECORD,
        user: null,
      };
    }
    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.create(
      email,
      firstName,
      lastName,
      passwordHash,
      STUDENT_ROLE,
    );

    return { error: null, user: user };
  };
};

const updateUser = usersData => {
  return async (id, userUpdate) => {
      const user = await usersData.getBy('u.id', id);
      if (!user) {
          return {
              error: errors.RECORD_NOT_FOUND,
              user: null,
          };
      }

      const updated = { ...user, ...userUpdate };
      /*eslint no-unused-vars: 'off'*/
      const _ = await usersData.update(updated);

      return { error: null, user: updated };
  };
};

const deleteUser = usersData => {
  return async (id) => {
      const userToDelete = await usersData.getBy('u.id', id);
      
      if (!userToDelete) {
          return {
              error: errors.RECORD_NOT_FOUND,
              user: null,
          };
      }

      const _ = await usersData.remove(userToDelete);

      return { error: null, user: userToDelete };
  };
};

export default {
  signInUser,
  getStudents,
  getTeachers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
