import pool from './pool.js';

const getAll = async (column, value) => {
    const sql = `
        SELECT *
        FROM sections
        WHERE ${column} = ?
        AND isDeleted = 0
        order by orderPosition
    `;

    return await pool.query(sql, [value]);
};

const getBy = async (column, value) => {
    const sql = `
     SELECT *
     FROM sections
     WHERE ${column} = ? 
     AND isDeleted = 0`;
    const result = await pool.query(sql, [value]);

    return result[0];
  };

const create = async (title, content, availableFrom, orderPosition, courseId) => {
    const sql = `
        INSERT INTO sections (title, content, availableFrom, orderPosition, courseId)
        VALUES (?, ?, ?, ?, ?)
    `;
  
    const result = await pool.query(sql, [title, content, availableFrom, orderPosition, courseId]);
  
    return {
        id: result.insertId,
        title: title,
        content: content,
        availableFrom: availableFrom,
        orderPosition: orderPosition,
        courseId: courseId,
    };
  };

  const update = async (section) => {
    const { id, title, content, availableFrom, orderPosition } = section;
    const sql = `
        UPDATE sections 
        SET title = ?,
        content = ?,
        availableFrom = ?,
        orderPosition = ?
        WHERE isDeleted = 0
        AND id = ?
    `;
  
    return await pool.query(sql, [title, content, availableFrom, orderPosition, id]);
  };

  const remove = async (column, value) => {
    const sql = `
        UPDATE sections
        SET isDeleted = 1
        WHERE ${column} = ?;`;

    return await pool.query(sql, [value]);
  };

const order = async (order1, order2) => {
  const sql = `
    UPDATE sections 
    SET orderPosition = CASE
      WHEN orderPosition BETWEEN ${order1}+1 AND ${order2}
        THEN orderPosition - 1
      WHEN orderPosition BETWEEN ${order2} AND ${order1}-1
        THEN orderPosition + 1
      ELSE ${order2}
    END
    WHERE orderPosition BETWEEN LEAST(${order1}, ${order2}) AND GREATEST(${order1}, ${order2})`;

  return await pool.query(sql);
};


export default {
    getAll,
    getBy,
    create,
    update,
    remove,
    order,
};
