import express from 'express';
import { PORT } from './config.js';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './src/auth/strategy.js';
import usersController from './src/controllers/users-controller.js';
import authController from './src/controllers/auth-controller.js';
import coursesController from './src/controllers/courses-controller.js';
import sectionsController from './src/controllers/sections-controller.js';

const app = express();

passport.use(jwtStrategy);
app.use(passport.initialize());
app.use(cors());
app.use(express.json());

app.use('/users', usersController);
app.use('/auth', authController);
app.use('/courses', coursesController);
app.use('/courses', sectionsController);

/*eslint no-unused-vars: 'off'*/
app.use((err, req, res, next) => {
 
  res.status(500).send({
    message: 'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});

app.all('*', (_, res) =>
  res.status(404).send({ message: 'Resource not found!' }),
);


app.listen(PORT, ()=> console.log(`App is listening on PORT ${PORT}`)); 